<?php

/**
 * Simple custom text box.
 */
class boxes_image_map extends boxes_box {
  /**
   * Implementation of boxes_box::options_defaults().
   */
  public function options_defaults() {
    
    $lang_name = language_list();
    
    $defaults = array(
        'box_id' => array(
        'value' => '',
      ),
      'image' => array(
        'fid' => '',
      ),
      'style' => array(
        'value' => '',
      ));
      
    foreach($lang_name as $lang){
      $defaults['markup_'.$lang->language] = array(
          'value' => '',
      );
    }
    
    $defaults['map_class'] =  array(
        'value' => '',
      );
    $defaults['map'] =  array(
        'value' => '',
      );
    
      return $defaults;
      
  }

  /**
   * Implementation of boxes_box::options_form().
   */
  public function options_form(&$form_state) {
      $validators = array();
      $validators['file_validate_is_image'] = array();
      $validators['file_validate_extensions'] = array('jpg jpeg gif png');
      if (is_array($this->options['box_id'])){
        $box_id = $this->options['box_id']['value'] ? $this->options['box_id']['value'] : NULL;
      }
      else {
        $box_id = $this->options['box_id'] ? $this->options['box_id'] : NULL;
      }
      if (empty($box_id)){
        $box_id = variable_get("boxes_images_next_id", 1);
        variable_set("boxes_images_next_id", $box_id + 1);
      }
      $form['box_id'] = array(
        '#type' => 'hidden',
        '#value' => $box_id,
      );
      
      $form['image'] = array(
        '#title' => t('Choose an image'),
        '#type' => 'managed_file',
        '#description' => t('Please add the image for this block.'),
        '#default_value' => $this->options['image'] ? $this->options['image'] : NULL,
        '#upload_location' => 'public://box_image_maps/',
       	'#upload_validators' => $validators,
        '#required' => TRUE,
      );
      $lang_name = language_list();
      foreach($lang_name as $lang){
        $form['markup_'.$lang->language] = array(
          '#type' => 'textarea',
          '#title' => t('Markup in @language', array('@language'=>$lang->name)),
          '#description' => t('The area tags of the image map, no map tag or image tag'),
          '#default_value' => $this->options['markup_'.$lang->language] ? $this->options['markup_'.$lang->language] : NULL,
          '#size' => 60,
          '#required' => TRUE,
        );
      }
      $form['map_class'] = array(
        '#type' => 'textfield',
        '#title' => t('Image class'),
        '#description' => t('The class to use for the image map'),
        '#default_value' => $this->options['map_class'] ? $this->options['map_class'] : NULL,
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => TRUE,
      );
      
      $form['map'] = array(
        '#type' => 'textfield',
        '#title' => t('Map name'),
        '#description' => t('The name to use for the image map'),
        '#default_value' => $this->options['map'] ? $this->options['map'] : NULL,
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => TRUE,
      );
      
      $form['#box_type'] = 'boxes_image_map';
      
      return $form;
  }

  /**
   * Implementation of boxes_box::render().
   */
  public function render() {
    global $language;
    $lang = $language->language;
    $title = isset($this->title) ? check_plain($this->title) : NULL;
    
    if ($this->options['image']){
/*   An associative array containing:
 *   - path: Either the path of the image file (relative to base_path()) or a
 *     full URL.
 *   - width: The width of the image (if known).
 *   - height: The height of the image (if known).
 *   - alt: The alternative text for text-based browsers. HTML 4 and XHTML 1.0
 *     always require an alt attribute. The HTML 5 draft allows the alt
 *     attribute to be omitted in some cases. Therefore, this variable defaults
 *     to an empty string, but can be set to NULL for the attribute to be
 *     omitted. Usually, neither omission nor an empty string satisfies
 *     accessibility requirements, so it is strongly encouraged for code calling
 *     theme('image') to pass a meaningful value for this variable.
 *     - http://www.w3.org/TR/REC-html40/struct/objects.html#h-13.8
 *     - http://www.w3.org/TR/xhtml1/dtds.html
 *     - http://dev.w3.org/html5/spec/Overview.html#alt
 *   - title: The title text is displayed when the image is hovered in some
 *     popular browsers.
 *   - attributes: Associative array of attributes to be placed in the img tag.
 */
      
      $map = $this->options['map'];
      $class = $this->options['map_class'];
      $file = file_load($this->options['image']);
      $variables = array(
        'path'=> $file->uri,
        'alt' => $title,
        'title' => $title,
        'attributes' => array(
          'usemap' => "#$map",
          'class' => $class,
        ),
      );
       $image = theme('image',$variables);
       $markup = $this->options['markup_'.$lang];
       $content = $image . '<map name="' . $map . '">' . $markup . '</map>';
       drupal_add_js('http://davidlynch.org/js/maphilight/jquery.maphilight.min.js.pagespeed.ce.lKjsi2gANH.js', array('type' => 'external'));
    }
    else {
      $content = '';
    }
    return array(
      'delta' => $this->delta, // Crucial.
      'title' => '',//$title,
      'subject' => '',//$title,
      'content' => $content,
    );
  }
}

